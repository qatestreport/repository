## ReflektiveRepository

This project is created with maven. No need to download or install any external dependency.


Some features

1. Test data can be store in Excel file(test data giving in each scripts with @Dataproviders)
2. It will take screen shot when script fails
3. It can generate report with screen shot
4. Used page object model concept to manage all the locators


### Please follow the steps to run tests.(for Question1 and Question2)

1. Update Chrome browser to latest version

2. From the root directory select `testsuite.xml` file and select option from the right click menu(`Run As` -> `TestNg Suite`)

3. Test Suite Contains Two Test Cases(ClearTrip and Amazon Scenario)

4. Test Cases are located under `src/test/java`

### Please follow the steps for Question4

1. After running tests, refresh the entire projects(Right click on the project -> `Refresh`)

2. Navigate to `test-output` folder in the root directory. 

3. Click on `index.html` file which is located under `/test-output/html/index.html` location

### Please follow the steps for Question3

1. Navigate to `src/test/java` and open `MonthlyBudgetExcelSheet` Class

2. Delete the file(`MONTHLY_BUDGET.xlsx`) from root directory if it already exists

3. Execute the scripts and refresh the project.

4. In the root directory search for newly created file(`MONTHLY_BUDGET.xlsx`)

5. Open the file to see the result



### Project Design information

1. Framework - [https://drive.google.com/file/d/1d3Ai_mxUo75jE2zmzZdqux6dTii5z3sH/view?usp=sharing]

2. Page object model design - [https://drive.google.com/file/d/1LUTlcDz4ZujN-F_RmtUxY2UVwMN3RiUn/view?usp=sharing]

3. Test Script desgn - [https://drive.google.com/file/d/1Z1lxOAEGHvcuY1oK0tipPAEq8pvG1nxr/view?usp=sharing]




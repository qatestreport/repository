package com.base;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseClass {
	
	// Wait until webElement to be clickable
	public void ExpliwaitUntilElementToBeClickable(WebDriver driver, long time, WebElement element) {

		new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public void ExpliwaitUntilElementToBeClickable(WebDriver driver, long time, By by) {

		new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeClickable(by));
	}

	// Text to be present by WebElement
	public void ExpliwaitUntilTextToBePresentByWebElement(WebDriver driver, long time, WebElement element, String text) {

		new WebDriverWait(driver, time).until(ExpectedConditions.textToBePresentInElement(element, text));
	}

	// Invisibility of Element by xpath
	public void ExpliwaitUntilInvisibilityOfElementByXpath(WebDriver driver,long time, String xpath) {

		new WebDriverWait(driver, time).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
	}

	// Invisibility of Element by css
	public void ExpliwaitUntilInvisibilityOfElementByCSS(WebDriver driver, long time, String css) {

		new WebDriverWait(driver, time).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(css)));
	}

	// Invisibility of Element by css
	public void ExpliwaitUntilInvisibilityOfElementWithTextByXpath(WebDriver driver, long time, String xpath, String text) {

		new WebDriverWait(driver, time).until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(xpath), text));
	}

	// Visibility of Element by WebElement
	public void ExpliwaitUntilVisibilityOfByWebElement(WebDriver driver, long time, WebElement element) {

		new WebDriverWait(driver, time).until(ExpectedConditions.visibilityOf(element));
	}
	
	// Visibility of Element by Locator
	public void ExpliwaitUntilVisibilityOfByLocator(WebDriver driver, long time, By by) {

		new WebDriverWait(driver, time).until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	// Presence of element located by xpath
	public void ExpliwaitUntilPresenceOfElementLocatedByXpath(WebDriver driver, long time, String xpath) {

		new WebDriverWait(driver, time).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	}

	// Presence of element located by css
	public void ExpliwaitUntilPresenceOfElementLocatedByCSS(WebDriver driver, long time, String css) {

		new WebDriverWait(driver, time).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(css)));
	}
	
	public void ExpliwaitUntilTextToBePresentInElementLocated(WebDriver driver, By by, String text, long time){
		
		new WebDriverWait(driver, time).until(ExpectedConditions.textToBePresentInElementLocated(by, text));
	}
	
	
	public void selectDropDownByVisibleText(WebElement element, String dropDownValue) {
		Select dropdownValue = new Select(element);
		dropdownValue.selectByVisibleText(dropDownValue);
	}
	
	public void moveToWebElement(WebDriver driver, WebElement ele) {
		new Actions(driver).moveToElement(ele).build().perform();
	}
	
	public void switchControleToNewlyOpenedTab(WebDriver driver) {
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		System.out.println(tabs);
		System.out.println(tabs.size());
		
	    driver.switchTo().window(tabs.get(tabs.size()-1));
	}

}

package com.base;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import com.utility.other.ScreenShot;

public class BaseTestClass {

	public WebDriver driver;

	@BeforeClass
	public void setUp() {

		System.setProperty("webdriver.chrome.driver", ".//src/main/resources/browser_drivers/MAC/chromedriver");
		
		DesiredCapabilities caps = new DesiredCapabilities();
		ChromeOptions options = new ChromeOptions();
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_settings.popups", 0);
		prefs.put("profile.default_content_setting_values.notifications", 1);
		options.setExperimentalOption("prefs", prefs);
		caps.setCapability(ChromeOptions.CAPABILITY, options);		
		
		driver = new ChromeDriver(options);
		driver.manage().window().setSize(new Dimension(1250, 780));
		
	}

	@AfterClass
	public void tearDown() {

		 driver.quit();
	}

	@AfterMethod
	public void screenShotOnFailure(ITestResult result) {

		if (!result.isSuccess())
			try {

				Reporter.setCurrentTestResult(result);
				ScreenShot.captureScreenshot(driver);
			}

			catch (Exception e) {

				System.out.println("" + e.getMessage());
			}

	}

}

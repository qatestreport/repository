package com.cleartrip.page.header;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.BaseClass;

public class ClearTripHeaderPage extends BaseClass {
	
	protected WebDriver driver;
	
	@FindBy(css = "a.ctBrand")
	WebElement clearTripLogo;
	
	@FindBy(css = "li.currencyMenuContainer")
	WebElement currencyDropDownMenu;
	
	@FindBy(css = "a.countryLink")
	WebElement selectCountryDropDownMenu;
	
	@FindBy(css = "li.userAccountMenuContainer")
	WebElement userAccountDropDownMenu;
	
	@FindBy(css = "div#ResultContainer_1_1 nav.powerFilters li.nonStops")
	WebElement nonStopFilterMenu;
	
	public ClearTripHeaderPage(WebDriver driver) {

		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}
	
	public ClearTripHeaderPage() {
		
	}
	
	public void selectNonStopFilter() {
		ExpliwaitUntilElementToBeClickable(driver, 10, nonStopFilterMenu);
		nonStopFilterMenu.click();
	}
	
	public void clickOnClearTripLogo() {
		clearTripLogo.click();
	}
	
	public void clickOnCurrencyDropDownMenu() {
		currencyDropDownMenu.click();
	}
	
	public void clickOnUserAccountDropDownMenu() {
		userAccountDropDownMenu.click();
	}

}

package com.cleartrip.page.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.page.searchresult.ClearTripFlightSearchResultPage;
import com.cleartrip.page.sidenav.ClearTripHomeSideNavPage;
import com.utility.other.PropertiesReaderFunction;

public class SearchFlightsPage extends ClearTripHomeSideNavPage {
	
	@FindBy(css = "#SearchForm h1")
	WebElement searchFlightsPageTitle;
	
	@FindBy(css = "#SearchForm h2")
	WebElement searchFlightsPageTitleDesc;
	
	@FindBy(id = "OneWay")
	WebElement oneWayCheckBox;
	
	@FindBy(id = "RoundTrip")
	WebElement roundTripCheckBox;
	
	@FindBy(id = "FromTag")
	WebElement flightsFromInputField;
	
	@FindBy(id = "ToTag")
	WebElement flightsToInputField;
	
	@FindBy(id = "DepartDate")
	WebElement departDateField;
	
	@FindBy(css = "label[for='DepartDate'] strong")
	WebElement departDateFieldLabelText;
	
	@FindBy(css = "div.datePicker input#FromDate ~ a i")
	WebElement departDateCalenderIcon;
	
	@FindBy(id = "ReturnDate")
	WebElement returnDateField;
	
	@FindBy(css = "div.datePicker input#ToDate ~ a i")
	WebElement returnDateCalenderIcon;
	
	@FindBy(id = "Adults")
	WebElement adultsDropDown;
	
	@FindBy(id = "Childrens")
	WebElement childrensDropDown;
	
	
	
	@FindBy(id = "SearchBtn")
	WebElement searchFlightsBtn;
	
	public SearchFlightsPage(WebDriver driver) {
		
		super(driver);
	}
	
	public SearchFlightsPage() {
		
	}
	
	public String getSearchFlightsPageTitle() {
		return searchFlightsPageTitle.getText();
	}
	
	public void openClearTripUrl() {
		driver.get(PropertiesReaderFunction.getEnvUrlFromPropertiesFile());
	}
	
	public void clickOnrRoundTripCheckBox() {
		roundTripCheckBox.click();
	}
	
	public void enterFlightsFrom(String from) throws Throwable {
		
		flightsFromInputField.sendKeys(from);
	}
	
	public void enterFlightsTo(String from) throws Throwable {
		
		flightsToInputField.sendKeys(from);
	}
	
	public void enterDepartOnDate() throws Throwable {
		
		departDateField.click();
		WebElement date = driver.findElement(By.xpath("(//table[@class='calendar'])[1]/tbody/tr/td/a[text()='16']"));
		ExpliwaitUntilElementToBeClickable(driver, 5000, date);
		date.click();
	}
	
	public String getTextFromDepartDateFieldLabelText() {
		return departDateFieldLabelText.getText();
	}
	
	public void enterReturnDate() throws Throwable {
		
		returnDateField.click();
		WebElement date = driver.findElement(By.xpath("(//table[@class='calendar'])[2]/tbody/tr/td/a[text()='17']"));
		ExpliwaitUntilElementToBeClickable(driver, 5000, date);
		date.click();
	}
	
	public void selectNoOfAdults(String noOfAddults) {
		selectDropDownByVisibleText(adultsDropDown, noOfAddults);
	}
	
	public void selectNoOfChildren(String noOfChildren) {
		selectDropDownByVisibleText(childrensDropDown, noOfChildren);
	}
	
	public void clickOnSearchFlightsBtn() {
		searchFlightsBtn.click();
		new ClearTripFlightSearchResultPage(driver).waitForClearTripFlightSearchResultPageReady();
	}

}

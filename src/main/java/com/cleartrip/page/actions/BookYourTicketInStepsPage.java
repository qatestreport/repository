package com.cleartrip.page.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.BaseClass;

public class BookYourTicketInStepsPage extends BaseClass {
	
	WebDriver driver;
	
	@FindBy(css = "div.container p.pageTitle")
	WebElement bookYourTicketPageTitle;
	
	/* Itinerary Section */
	
	@FindBy(css = "div#itineraryOpen h2")
	WebElement itineraryStepHeading;
	
	@FindBy(css = "div#itineraryOpen p.subHead")
	WebElement itineraryStepSubHeading;
	
	@FindBy(id = "itineraryBtn")
	WebElement itineraryContinueBookingBtn;
	
	/* Email address Section */
	
	@FindBy(id = "username")
	WebElement emailIdField;
	
	@FindBy(id = "LoginContinueBtn_1")
	WebElement emailAddressContinueBtn;
	
	/* Travellers */
	
	@FindBy(css = "div#travellerOpen h2")
	WebElement travellersStepHeading;
	
	@FindBy(id = "AdultTitle1")
	WebElement adultTitleDropDown;
	
	@FindBy(id = "AdultFname1")
	WebElement adultFirstName;
	
	@FindBy(id = "AdultLname1")
	WebElement adultLastName;
	
	@FindBy(id = "ChildTitle1")
	WebElement childTitleDropDown;
	
	@FindBy(id = "ChildFname1")
	WebElement childFirstName;
	
	@FindBy(id = "ChildLname1")
	WebElement childLastName;
	
	@FindBy(id = "ChildDobDay1")
	WebElement dayOfDOB;
	
	@FindBy(id = "ChildDobMonth1")
	WebElement monthOfDOB;
	
	@FindBy(id = "ChildDobYear1")
	WebElement yearOfDOB;
	
	@FindBy(id = "mobileNumber")
	WebElement mobileNumberField;
	
	@FindBy(id = "travellerBtn")
	WebElement travellersContinueBtn;
	
	/* Payment */
	
	@FindBy(id = "paymentSubmit")
	WebElement makePaymentBtn;
	
	public BookYourTicketInStepsPage(WebDriver driver) {

		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}
	
	public void waitForItinerarySectionReady() {
		ExpliwaitUntilElementToBeClickable(driver, 30, itineraryContinueBookingBtn);
	}
	
	public String getTextFromBookYourTicketPageTitle() {
		return bookYourTicketPageTitle.getText();
	}
	
	public void clickOnItineraryContinueBookingBtn() {
		moveToWebElement(driver, itineraryContinueBookingBtn);
		itineraryContinueBookingBtn.click();		
		ExpliwaitUntilElementToBeClickable(driver, 10, emailAddressContinueBtn);
	}
	
	public String getTextFromItineraryStepHeading(){
		return itineraryStepHeading.getText();
	}
	
	/* Email Address section */
	
	public void enterEmailId(String email) {
		ExpliwaitUntilVisibilityOfByWebElement(driver, 30, emailIdField);
		emailIdField.sendKeys(email);
	}
	
	public void clickOnEmailAddressSectionContinueBtn() {
		emailAddressContinueBtn.click();
	}
	
	public void enterDetailsInEmailAddressSection(String email) {
		
		enterEmailId(email);
		clickOnEmailAddressSectionContinueBtn();
	}
	
	/* Travellers section */
	
	public String getTextFromTravellersStepHeading() {
		return travellersStepHeading.getText();
	}
	
	public void enterAdultDetails(String title, String firstName, String lastName) {
		ExpliwaitUntilVisibilityOfByWebElement(driver, 30, adultTitleDropDown);
		selectDropDownByVisibleText(adultTitleDropDown, title);
		adultFirstName.sendKeys(firstName);
		adultLastName.sendKeys(lastName);
	}
	
	public void enterChildDetails(String title, String childFN, String childLN, String day, String month, String year) {
		ExpliwaitUntilVisibilityOfByWebElement(driver, 30, childTitleDropDown);
		selectDropDownByVisibleText(childTitleDropDown, title);
		childFirstName.sendKeys(childFN);
		childLastName.sendKeys(childLN);
		
		selectDropDownByVisibleText(dayOfDOB, day);
		selectDropDownByVisibleText(monthOfDOB, month);
		selectDropDownByVisibleText(yearOfDOB, year);	
	}
	
	public void enterMobileNumber(String mob) {
		mobileNumberField.sendKeys(mob);
	}
	
	public void clickOnTravellersContinueBtn() {
		travellersContinueBtn.click();
		ExpliwaitUntilElementToBeClickable(driver, 30, makePaymentBtn);
	}

	
}

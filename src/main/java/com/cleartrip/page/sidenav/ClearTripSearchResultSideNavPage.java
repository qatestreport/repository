package com.cleartrip.page.sidenav;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.page.header.ClearTripHeaderPage;

public class ClearTripSearchResultSideNavPage extends ClearTripHeaderPage {
	
//	protected WebDriver driver;
	
	@FindBy(css = "div#ResultContainer_1_1 a#closeFilter ~ p a.resetLink")
	WebElement showAllLink;
	
	@FindBy(css = "div[data-block-type='departureTime'] input[value='0_8'] ~ label span")
	WebElement departureTimeEarlyMorningValue;
	
	@FindBy(css = "div[data-block-type='departureTime'] input[value='8_12'] ~ label span")
	WebElement departureTimeMorningValue;
	
	@FindBy(css = "div[data-block-type='departureTime'] input[value='12_16'] ~ label span")
	WebElement departureTimeMidDayValue;
	
	@FindBy(css = "div[data-block-type='arrivalTime'] input[value='20_24'] ~ label span")
	WebElement returnTimeNightValue;
	
	
	public ClearTripSearchResultSideNavPage(WebDriver driver) {

//		this.driver = driver;
		// This initElements method will create all WebElements
//		PageFactory.initElements(driver, this);
		super(driver);
	}
	
	public ClearTripSearchResultSideNavPage() {
		
	}
	
	public void selectEarlyMorningDepartureTimeFilter() {
		ExpliwaitUntilElementToBeClickable(driver, 10, departureTimeEarlyMorningValue);
		departureTimeEarlyMorningValue.click();
	}
	
	public void selectNightReturnTimeFilter() {
		moveToWebElement(driver, returnTimeNightValue);
		returnTimeNightValue.click();
	}
	
	
	
	
	

}

package com.cleartrip.page.sidenav;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.page.header.ClearTripHeaderPage;

public class ClearTripHomeSideNavPage extends ClearTripHeaderPage {
	
	@FindBy(css = "#Home ul li.flightApp")
	WebElement flightsMenu;
	
	@FindBy(css = "#Home ul li.hotelApp")
	WebElement hotelsMenu;
	
	@FindBy(css = "#Home ul li.localApp")
	WebElement activitiesMenu;
	
	@FindBy(css = "#Home ul li.trainsApp")
	WebElement trainsMenu;
	
	@FindBy(css = "#Home ul li.hideB2B")
	WebElement flightDealsMenu;
	
	@FindBy(css = "#Home ul li.hideB2B a[href='/mobile']")
	WebElement mobileMenu;
	
	public ClearTripHomeSideNavPage(WebDriver driver) {
		super(driver);

	}
	public ClearTripHomeSideNavPage() {
		
	}
	public void clickOnFlightsMenu() {
		flightsMenu.click();
	}
	
	public void clickOnHotelsMenu() {
		hotelsMenu.click();
	}

}

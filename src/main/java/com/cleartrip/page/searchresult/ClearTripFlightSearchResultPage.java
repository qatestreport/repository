package com.cleartrip.page.searchresult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.cleartrip.page.sidenav.ClearTripSearchResultSideNavPage;

public class ClearTripFlightSearchResultPage extends ClearTripSearchResultSideNavPage {
	
	@FindBy(xpath = "(//form[@id='flightForm'])[2]/section[@class='resultsContainer']/div/div/button")
	WebElement bookBtn;
	
	@FindBy(xpath = "(//td[@id='BackToRtSpT']/a/strong)[2]")
	WebElement allAirlineTabText;
	
	public ClearTripFlightSearchResultPage(WebDriver driver) {

		super(driver);
	}
	
	public ClearTripFlightSearchResultPage() {
		
	}
	
	public void waitForClearTripFlightSearchResultPageReady() {
		ExpliwaitUntilElementToBeClickable(driver, 40, bookBtn);
	}
	
	public String getTextFromAllAirlineTab() {
		return allAirlineTabText.getText();
	}
	
	/*This DYNAMIC function select lastly departing flight from the list of flight*/
	public void selectLastlyDepartingFlights() {
		
		ExpliwaitUntilElementToBeClickable(driver, 10, By.cssSelector("div[data-leg='2']"));
		
		List<WebElement> myElements = driver.findElements(By.cssSelector("div[data-leg='2'] ul.flights table th.depart"));		
		List<String> al = new ArrayList<String>();
		
		for(WebElement e : myElements) {
			  String departTime = e.getText();
			  al.add(departTime);
		}
		
		Collections.sort(al, Collections.reverseOrder());
		
		String firstElement = al.get(0);
		WebElement lastDepartFlight = driver.findElement(By.xpath("//div[@data-leg='2']//li//th[text()='"+firstElement+"']"));
		
		moveToWebElement(driver, lastDepartFlight);
		lastDepartFlight.click();
		
	}	
	
	public void clickOnBookBtn() {
		moveToWebElement(driver, bookBtn);
		bookBtn.click();
	}
	
}

package com.utility.other;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

public class ScreenShot {
	
	public static void captureScreenshot(WebDriver driver) {

		try {

			String failureImageFileName = new SimpleDateFormat(
					"MM-dd-yyyy HH-mm-ss-S").format(new GregorianCalendar()
					.getTime())
					+ ".png";
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("./test-output/html/Screenshots/"+ failureImageFileName + ""));

			String paths = ("<img width=\"550\" height=\"350\" alt=\"alternativeName\" title=\"title\" src=\"./Screenshots/"
					+ failureImageFileName +"\" alt=\"\"/></img>");

			Reporter.log("<br>" + paths);
			System.out.println("Screenshot taken");
			
		} catch (Exception e) {

			System.out.println("Exception while taking screenshot "
					+ e.getMessage());
		}
	}


}

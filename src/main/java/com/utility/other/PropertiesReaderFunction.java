package com.utility.other;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReaderFunction {
	
	/*
	 * get property value from properties file. Mainly using to store app_url, env specific 
	 * values etc
	 */
	
	static String URL;

	private static String getEnvironmentProperty(String PropertyNam, String filePath) {

		InputStream inputStream = null;
		String PropNam = null;

		try {

			Properties prop = new Properties();
			inputStream = new FileInputStream(filePath);

			prop.load(inputStream);
			PropNam = prop.getProperty(PropertyNam);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return PropNam;

	}
	
	/*
	 * Get ENV proprty Value
	 */
	private static String getEnvPropertyValue() {
		
		return getEnvironmentProperty("ENV", "config/config.properties");
	}
	
	/* 
	 * Get environment specific url
	 * 
	 */
	public static String getEnvUrlFromPropertiesFile() {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("config/config.properties");

			// load a properties file
			prop.load(input);

			switch (getEnvPropertyValue()) {

			case "PROD":

				URL = prop.getProperty("prod_url");
				break;

			case "DEV":

				URL = prop.getProperty("dev_url");
				break;
			/*
			 * You can add any number of environment details here..!!
			 */
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return URL;

	}


}

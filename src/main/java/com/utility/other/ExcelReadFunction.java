package com.utility.other;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReadFunction {
	
	static XSSFWorkbook wb;
	static XSSFSheet sheet;
	static XSSFCell Cell;
	
	/**
	 * 
	 * @param sheetNum
	 * @param Row
	 * @param Col
	 * @return Data from an excel cell
	 */
	public static String getCellData(int sheetNum,int Row, int Col) {

		sheet = wb.getSheetAt(sheetNum);
		DataFormatter df = new DataFormatter();
		Cell = sheet.getRow(Row).getCell(Col);

		String finalData = df.formatCellValue(Cell);

		return finalData;

	}
	
	/**
	 * 
	 * @param sTestCaseName
	 * @param sheet_nu
	 * @return Row that contains test data
	 * @throws Throwable 
	 */
	public static int getRowContains(String sTestCaseName, int sheet_nu, String filePath) {

		int i;

		try {

			int rowcount = rowCount(sheet_nu, filePath);
			System.out.println("Function row count" + rowcount);

			for (i = 0; i < rowcount; i++) {

				if (ExcelReadFunction.getCellData(sheet_nu, i, 0).equals(
						sTestCaseName)) {

					break;
				}

			}

			return i;

		} catch (Exception e) {

			throw (e);

		}

	}
	
	/**
	 * 
	 * @param FilePath
	 * @param Sheet_no
	 * @param TestCaseRow
	 * @return Test data Array - [1][x]
	 * @throws Exception
	 */
	public static Object[][] getTableArray(String FilePath, int Sheet_no, int TestCaseRow) throws Exception {

		String[][] tabArray = null;

		try {

			File src = new File(FilePath);
			FileInputStream ExcelFile = new FileInputStream(src);

			wb = new XSSFWorkbook(ExcelFile);

			sheet = wb.getSheetAt(Sheet_no);

			int no_of_col = sheet.getRow(TestCaseRow).getLastCellNum();
			//System.out.println("Array fn+no of column" + no_of_col);

			int act_col = no_of_col - 1;

			int startCol = 1;

			int ci = 0, cj = 0;

			tabArray = new String[1][act_col];

			for (int i = startCol; i < no_of_col; i++, cj++) {

				tabArray[ci][cj] = getCellData(Sheet_no,TestCaseRow, i);

				System.out.println(tabArray[ci][cj]);
			}

		}

		catch (FileNotFoundException ee) {

			System.out.println("Could not read the Excel sheet");
			ee.printStackTrace();
		}

		catch (IOException e) {

			System.out.println("Could not read the Excel sheet");
			e.printStackTrace();
		}

		return (tabArray);

	}

	public String getStringdata(int sheetNum, int Row, int Col) {

		sheet = wb.getSheetAt(sheetNum);

		String dataString = sheet.getRow(Row).getCell(Col).getStringCellValue().toString();

		return dataString;
	}

	public String getNumericdata(int sheetNum, int rownum, int colno) {

		sheet = wb.getSheetAt(sheetNum);

		double dataString = sheet.getRow(rownum).getCell(colno).getNumericCellValue();

		Double d = new Double(dataString);
		int i = d.intValue();

		String OrgDta = Integer.toString(i);

		return OrgDta;
	}

	public static void WriteDataToExcel(int SheetNum, int Row, int Col,
			String data_to_set, String testdata1) throws Throwable {

		try {

			// File src = new File(testdata1);
			FileInputStream fis = new FileInputStream(testdata1);
			XSSFWorkbook wb = new XSSFWorkbook(fis);

			XSSFSheet sheet = wb.getSheetAt(SheetNum);

			//setting row
			Row row = sheet.getRow(Row);
			Cell cell = row.createCell(Col);

			cell.setCellValue(data_to_set);

			//Paste data into an excell file
			FileOutputStream outFile = new FileOutputStream(new File(testdata1));
			wb.write(outFile);

			outFile.close();
			wb.close();
			fis.close();

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static int rowCount(int sheetNum, String filePath) {
		
		int row = 0;

		try {
			File src = new File(filePath);
			FileInputStream fis = new FileInputStream(src);

			wb = new XSSFWorkbook(fis);

			row = wb.getSheetAt(sheetNum).getLastRowNum();
			row = row + 1;

		} catch (Exception e) {
			
			System.out.println("Error:" + e);
		}

		return row;
	}

}

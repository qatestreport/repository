package com.utility.dataprovider;

import org.testng.annotations.DataProvider;

import com.utility.other.ExcelReadFunction;

public class DataProviderClass {
	
	static String fileLoc =".//src/test/resources/test_data/test_data_file.xlsx";
	
	@DataProvider(name = "clearTripFlightSearch")
	public static Object[][] clearTripFlightSearch() throws Throwable {

		int rows = ExcelReadFunction.getRowContains("ClearTrip", 0, fileLoc);
		Object[][] testObjArray = ExcelReadFunction.getTableArray(fileLoc, 0, rows);

		return testObjArray;

	}

	@DataProvider(name = "bookAGiftFromAmazon")
	public static Object[][] bookAGiftFromAmazon() throws Throwable {

		int rows = ExcelReadFunction.getRowContains("Amazon", 0, fileLoc);
		Object[][] testObjArray = ExcelReadFunction.getTableArray(fileLoc, 0, rows);

		return testObjArray;

	}
}

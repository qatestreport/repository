package com.amazon.page.login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amazon.page.productdetails.AmazonProductOrderingPage;
import com.base.BaseClass;

public class AmazonLoginPage extends BaseClass {
	
	WebDriver driver;
	
	@FindBy(id = "ap_email")
	WebElement emailIdField;
		
	@FindBy(id = "continue")
	WebElement continueBtn;
	
	@FindBy(id = "ap_password")
	WebElement passwordField;
	
	@FindBy(id = "signInSubmit")
	WebElement loginBtn;
	
	public AmazonLoginPage(WebDriver driver) {

		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}
	
	public void enterEmailOrPhone(String email) {
		emailIdField.sendKeys(email);
		continueBtn.click();
		ExpliwaitUntilElementToBeClickable(driver, 20, loginBtn);
		ExpliwaitUntilVisibilityOfByWebElement(driver, 20, loginBtn);
	}
	
	public void enterPassword(String pas) {
		
		passwordField.sendKeys(pas);
		loginBtn.click();
		new AmazonProductOrderingPage(driver).waitForAmazonProductOrderingPageReady();
	}


}

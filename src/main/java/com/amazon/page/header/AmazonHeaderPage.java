package com.amazon.page.header;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.BaseClass;

public class AmazonHeaderPage extends BaseClass {
	
	WebDriver driver;
	
	@FindBy(id = "searchDropdownBox")
	WebElement productTypeDropDown;
	
	@FindBy(id = "twotabsearchtextbox")
	WebElement productSearchInputField;
	
	@FindBy(css = "div.nav-search-submit input[value='Go']")
	WebElement productSearchBtn;
	
	public AmazonHeaderPage(WebDriver driver) {

		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}
	
	public void openAmazonUrl() {
		driver.get("https://www.amazon.in");
	}
	
	public void searchProduct(String productType, String input) {
		selectDropDownByVisibleText(productTypeDropDown, productType);
		productSearchInputField.sendKeys(input);
		productSearchBtn.click();
		
	}
	
	
	
	

	
	

}

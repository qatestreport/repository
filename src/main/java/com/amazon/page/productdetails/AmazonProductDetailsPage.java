package com.amazon.page.productdetails;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.BaseClass;

public class AmazonProductDetailsPage extends BaseClass {
	
	WebDriver driver;
	
	@FindBy(xpath = "//div[@id='tmmSwatches']//a/span[contains(text(),'Paper')]")
	WebElement paperBackBtn;
	
	@FindBy(id = "buy-now-button")
	WebElement buyNowBtn;
	
	public AmazonProductDetailsPage(WebDriver driver) {

		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}
	
	public void clickOnPaperBackBtn() {
		paperBackBtn.click();
		switchControleToNewlyOpenedTab(driver);
		ExpliwaitUntilElementToBeClickable(driver, 20, buyNowBtn);
	}
	
	public void clickOnBuyNowBtn() {
		moveToWebElement(driver, buyNowBtn);
		buyNowBtn.click();
		
	}
	
	
	
	
 

}

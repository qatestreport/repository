package com.amazon.page.productdetails;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.BaseClass;

public class AmazonProductOrderingPage extends BaseClass {
	
	WebDriver driver;
	
	@FindBy(css = "div#address-book-entry-0 div.ship-to-this-address a")
	WebElement deliverToThisAddressBtn;
	
	@FindBy(id = "continue-top")
	WebElement paymentMethodContinueBtn;
	
	public AmazonProductOrderingPage(WebDriver driver) {
		
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);
		
	}
	
	
	public void waitForAmazonProductOrderingPageReady() {
		ExpliwaitUntilElementToBeClickable(driver, 20, deliverToThisAddressBtn);
	}
	
	public void clickOnDeliverToThisAddressBtn() {
		
		deliverToThisAddressBtn.click();
		ExpliwaitUntilElementToBeClickable(driver, 20, paymentMethodContinueBtn);
		ExpliwaitUntilVisibilityOfByWebElement(driver, 20, paymentMethodContinueBtn);
	}	

}

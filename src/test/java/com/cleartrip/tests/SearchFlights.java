package com.cleartrip.tests;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.base.BaseTestClass;
import com.cleartrip.page.actions.BookYourTicketInStepsPage;
import com.cleartrip.page.actions.SearchFlightsPage;
import com.cleartrip.page.searchresult.ClearTripFlightSearchResultPage;
import com.utility.dataprovider.DataProviderClass;
import com.utility.other.ScreenShot;

public class SearchFlights extends BaseTestClass {
	
	SearchFlightsPage flights;
	ClearTripFlightSearchResultPage flightSearchResult;
	BookYourTicketInStepsPage bookTicket;
	
	@Test(dataProvider = "clearTripFlightSearch", dataProviderClass = DataProviderClass.class)
	public void _1SearchFlights(String from, String to, String noOfAdults, String noOfChild, String email,
			String adultTitle, String adultFN, String adultLN, String childTitle, String childFN, String childLN,
			String dayDOB, String monthDOB, String yearDOB, String mobile) throws Throwable {
		
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		
		flights = new SearchFlightsPage(driver);
		flights.openClearTripUrl();
		Assert.assertEquals("Search flights", flights.getSearchFlightsPageTitle());
		
		flights.clickOnrRoundTripCheckBox();
		Reporter.log("<br>Clicked on round trip checkbox");
		
		flights.enterFlightsFrom(from);
		Reporter.log("<br>Entered Flights From");
		
		flights.enterFlightsTo(to);
		Reporter.log("<br>Entered Flights To");
		
		flights.enterDepartOnDate();
		Assert.assertEquals("Depart on", flights.getTextFromDepartDateFieldLabelText());
		Reporter.log("<br>Entered DepartOn date");
		
		flights.enterReturnDate();
		Reporter.log("<br>Entered Return date");
		
		flights.selectNoOfAdults(noOfAdults);
		flights.selectNoOfChildren(noOfChild);
		ScreenShot.captureScreenshot(driver);
		flights.clickOnSearchFlightsBtn();
		Reporter.log("<br>Clicked on search flights button");
		
	}
	
	@Test(dataProvider = "clearTripFlightSearch", dataProviderClass = DataProviderClass.class)
	public void _2SelectFlightDetails(String from, String to, String noOfAdults, String noOfChild, String email,
			String adultTitle, String adultFN, String adultLN, String childTitle, String childFN, String childLN,
			String dayDOB, String monthDOB, String yearDOB, String mobile) {
		
		flightSearchResult = new ClearTripFlightSearchResultPage(driver);
//		Assert.assertEquals("All airlines", flightSearchResult.getTextFromAllAirlineTab());
		flightSearchResult.selectEarlyMorningDepartureTimeFilter();
		Reporter.log("<br>Clicked on EarlyMorning Departure filter");
		
		flightSearchResult.selectNightReturnTimeFilter();
		Reporter.log("<br>Clicked on NightReturn filter");
		
		flightSearchResult.selectNonStopFilter();
		Reporter.log("<br>Clicked on NonStop filter");
		
		/*This DYNAMIC function select lastly departing function*/
		flightSearchResult.selectLastlyDepartingFlights();
		Reporter.log("<br>selected lastly departed flights");
		
		ScreenShot.captureScreenshot(driver);
		flightSearchResult.clickOnBookBtn();
		Reporter.log("<br>Clicked on book button");
		
	}
	
	@Test(dataProvider = "clearTripFlightSearch", dataProviderClass = DataProviderClass.class)
	public void _3BookFlightTicket(String from, String to, String noOfAdults, String noOfChild, String email,
			String adultTitle, String adultFN, String adultLN, String childTitle, String childFN, String childLN,
			String dayDOB, String monthDOB, String yearDOB, String mobile) {
		
		bookTicket = new BookYourTicketInStepsPage(driver);
		Assert.assertEquals("Book in four simple steps", bookTicket.getTextFromBookYourTicketPageTitle());
		Assert.assertEquals("1\nItinerary", bookTicket.getTextFromItineraryStepHeading());
		
		bookTicket.clickOnItineraryContinueBookingBtn();
		ScreenShot.captureScreenshot(driver);
		Reporter.log("<br>Entered Itinerary Information");
		
		bookTicket.enterDetailsInEmailAddressSection(email);
		ScreenShot.captureScreenshot(driver);
		Reporter.log("<br>Entered Email Address Section information");
		
		bookTicket.enterAdultDetails(adultTitle, adultFN, adultLN);
		bookTicket.enterChildDetails(childTitle, childFN, childLN, dayDOB, monthDOB, yearDOB);
		bookTicket.enterMobileNumber(mobile);
		ScreenShot.captureScreenshot(driver);
		bookTicket.clickOnTravellersContinueBtn();
		Reporter.log("<br>Entered travellers Information");
		
	}

}

package com.amazon.tests;

import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.amazon.page.home.AmazonHomePage;
import com.amazon.page.login.AmazonLoginPage;
import com.amazon.page.productdetails.AmazonProductDetailsPage;
import com.amazon.page.productdetails.AmazonProductOrderingPage;
import com.base.BaseTestClass;
import com.utility.dataprovider.DataProviderClass;
import com.utility.other.ScreenShot;

public class BookAGiftFromAmazon extends BaseTestClass {
	
	AmazonHomePage home;
	AmazonProductDetailsPage prodDetails;
	AmazonLoginPage login;
	AmazonProductOrderingPage order;
	
	@Test(dataProvider = "bookAGiftFromAmazon", dataProviderClass = DataProviderClass.class)
	public void _1openAmazonAndSearchProduct(String prodType, String prodName, String userName, String Pas) throws Throwable {
		
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		
		home = new AmazonHomePage(driver);
		home.openAmazonUrl();
		home.searchProduct(prodType, prodName);
		Reporter.log("<br>Searched items in amazon");
		
	}
	
	@Test(dataProvider = "bookAGiftFromAmazon", dataProviderClass = DataProviderClass.class)
	public void _2addProductToCart(String prodType, String prodName, String userName, String pas) {
		
		ScreenShot.captureScreenshot(driver);
		driver.findElement(By.xpath("//ul[@id='s-results-list-atf']/li[1]//a/img")).click();
		Reporter.log("<br>clicked on product item");
		home.switchControleToNewlyOpenedTab(driver);
		
		prodDetails = new AmazonProductDetailsPage(driver);
		prodDetails.clickOnPaperBackBtn();
		prodDetails.clickOnBuyNowBtn();
		ScreenShot.captureScreenshot(driver);
		Reporter.log("<br>clicked on buy now button");
		
		login = new AmazonLoginPage(driver);
		login.enterEmailOrPhone(userName);
		login.enterPassword(pas);
		
		order = new AmazonProductOrderingPage(driver);
		ScreenShot.captureScreenshot(driver);
		order.clickOnDeliverToThisAddressBtn();
		Reporter.log("<br>clicked on 'deliver to this address' button");
	}

}

package com.excelsheet.test;

import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class MonthlyBudgetExcelSheet {

	public static void main(String[] args) throws Throwable {
		// TODO Auto-generated method stub
		/*
		 * After execution of this script, a new file will generate in the root
		 * directory
		 */

		// create a workbook
		XSSFWorkbook workbook = new XSSFWorkbook();
		// create a sheet
		XSSFSheet sheet = workbook.createSheet("MONTHLY BUDGET");

		Object[][] bookData = { { "ID", "PURPOSE", "AMOUNT" },
				{ 1, "Cab fares for the trip", 534 },
				{ 2, "Gift for Sandhya", 240 },
				{ 3, "Food expenses in Delhi", 2500 }, { 4, "Buffer", 1500 },
				{ 5, "Trip to Delhi", 18108 } };

		int rowCount = 0;

		for (Object[] aBook : bookData) {
			Row row = sheet.createRow(++rowCount);

			int columnCount = 0;

			for (Object field : aBook) {
				Cell cell = row.createCell(++columnCount);
				if (field instanceof String) {
					cell.setCellValue((String) field);
				} else if (field instanceof Integer) {
					cell.setCellValue((Integer) field);
				}
			}

		}

		Row row = sheet.createRow(++rowCount);
		Cell cellTotal = row.createCell(2);
		cellTotal.setCellValue("TOTAL");

		// Setting cell formula and cell type
		Cell cell = row.createCell(3);
		cell.setCellFormula("SUM(D3:D7)");
		cell.setCellType(Cell.CELL_TYPE_FORMULA);

		try (FileOutputStream outputStream = new FileOutputStream(
				"MONTHLY_BUDGET.xlsx")) {
			workbook.write(outputStream);
		}

		workbook.close();
	}

}
